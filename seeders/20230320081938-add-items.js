'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    
    await queryInterface.bulkInsert('Todos',[
      {
        title: "todo 1",
        status: "active",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "todo 2",
        status: "active",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "todo 3",
        status: "active",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "todo 4",
        status: "active",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "todo 5",
        status: "active",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ])
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {

    await queryInterface.bulkDelete('Todos', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
