const app = require("../app");
const request = require("supertest");


describe("API /todos", () => {

    it("test get /todos", (done) => {
        request(app)
            .get("/todos")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const firstData = response.body[0]
                expect(firstData.id).toBe(4)
                expect(firstData.title).toBe('todo 4')
                expect(firstData.status).toBe("active")
                done();
            })
            .catch(done)
    })

    it("test get /todos/:id", (done) => {
        
        request(app)
            .get("/todos/2")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const data = response.body;
                expect(data.id).toBe(2)
                expect(data.title).toBe('todo 2')
                expect(data.status).toBe('inactive')
                done();
            })
            .catch(done)
    })

    it("test post /todos", (done) => {

        request(app)
            .post("/todos")
            .send({title: "todo 20", status: "active"})
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .then(response => {
                const data = response.body;
                expect(data.title).toBe("todo 20")
                expect(data.status).toBe("active")
                expect(data.title).toMatch(/odo/)
                done();
            })
            .catch(done)
    })

    it("delete post /todos/:id", (done) => {

        request(app)
            .delete("/todos/6")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const data = response.body;
                expect(data.message).toBe("Deleted Successfully")
                done();
            })
            .catch(done)
    }) 
})
